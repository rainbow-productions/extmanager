void function(){
    chrome.management.getAll(extensions => {
      function handleExtension(extension) {
        const nameLink = `<b><a title="${extension.description}">${extension.name}</a></b>`;
        const message = `
          <p>${nameLink} (${extension.id}):</p>
          <button onclick="disableExtension('${extension.id}')">Disable</button>
        `;
        document.body.innerHTML += message;
      }
  
      document.body.innerHTML = "";
      document.writeln(`
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap" rel="stylesheet">
        <style>
          body {
            font-family: 'Montserrat', sans-serif;
          }
          a {
            text-decoration: none;
            color: blue;
          }
          p {
            margin: 0px;
          }
        </style>
        <h1 style="text-align: center">Extension Panel</h1>
      `);
  
      extensions.forEach(extension => {
        handleExtension(extension);
      });
    });
  
    window.disableExtension = function(extensionId) {
      chrome.management.setEnabled(extensionId, false, () => {
        document.body.innerHTML += `
          <p>Extension with ID ${extensionId} has been forcefully disabled.</p>
        `;
      });
    }
  }();