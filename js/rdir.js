function redirect(page) {
    if (page === 'panel') {
        window.location.replace('/panel.html')
    } else {
        window.alert('Invalid page: ' + page);
    }
}