function extensionPanelExecute() {
    document.addEventListener('DOMContentLoaded', () => {
        const extensionList = document.getElementById('extensionList');
      
        chrome.management.getAll(extensions => {
          if (extensions.length > 0) {
            extensions.forEach(extension => {
              const { id, name, icon } = extension;
      
              const item = document.createElement('div');
              item.className = 'extension-item';
      
              const iconImg = document.createElement('img');
              iconImg.className = 'extension-icon';
              iconImg.src = icon ? icon : 'placeholder.png';
              item.appendChild(iconImg);
      
              const details = document.createElement('div');
              details.className = 'extension-details';
      
              const nameEl = document.createElement('div');
              nameEl.className = 'extension-name';
              nameEl.textContent = name;
              details.appendChild(nameEl);
      
              const idEl = document.createElement('div');
              idEl.className = 'extension-id';
              idEl.textContent = id;
              details.appendChild(idEl);
      
              item.appendChild(details);
              extensionList.appendChild(item);
            });
          } else {
            const emptyMessage = document.createElement('div');
            emptyMessage.className = 'empty-message';
            emptyMessage.textContent = 'No extensions found.';
            extensionList.appendChild(emptyMessage);
          }
        });
      });
}