function checkBrowser() {
    var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    if (!isChrome) {
        window.location.replace = "incompatible.html";
    } else {}
}